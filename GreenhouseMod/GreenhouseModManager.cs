﻿using Plukit.Base;
using Staxel.EntityActions;
using Staxel.Farming;
using Staxel.Logic;
using Staxel.Tiles;
using System;
using System.Collections.Generic;
using System.Reflection.Emit;
using Harmony;
using Harmony.ILCopying;
using NimbusFox.FoxCore;
using Staxel.Items;
using Staxel.Modding;

namespace GreenhouseMod
{
	public class GreenhouseModManager : IModHookV3
	{
		public static GreenhouseModManager Instance { get; private set; }

		public PlantLogic PlantLogic { get; private set; }

	    internal readonly Fox_Core FxCore;

		public GreenhouseModManager()
		{
			Instance = this;
			PlantLogic = new PlantLogic();
		
		    FxCore = new Fox_Core("Barrybadpak", "GreenHouse", "V0.1");
		    FxCore.PatchController.Add(typeof(PlantConfiguration), "CheckPlantability", null, null, typeof(PlantLogic), "CheckPlantability");

            FxCore.PatchController.Override(typeof(CheckPlantGrowthStageAction), "Start", GetType(), nameof(CheckPlantStart), CheckPlantStartTranspiler);

            FxCore.PatchController.Add(typeof(FarmingDatabase), "DailyVisit", GetType(), nameof(BeforeDailyVisit));
        }

	    internal static void CheckPlantStart(Entity entity, EntityUniverseFacade facade) {
            Instance.PlantLogic.CheckPlantGrowthStage(entity, facade);
	    }

	    internal static IEnumerable<CodeInstruction> CheckPlantStartTranspiler(
	        IEnumerable<CodeInstruction> instructions) {
	        var replacementMethod = AccessTools.Method(typeof(GreenhouseModManager), nameof(CheckPlantStart));
            var methodIlInstructions = MethodBodyReader.GetInstructions(new DynamicMethod(Guid.NewGuid().ToString(), null, new Type[0]).GetILGenerator(), replacementMethod);
            var replacementMethodInstructions = new List<CodeInstruction>();

            foreach (var methodIlInstruction in methodIlInstructions) {
                replacementMethodInstructions.Add(methodIlInstruction.GetCodeInstruction());
            }

            return replacementMethodInstructions;
        }

	    internal static bool BeforeDailyVisit(Blob plantBlob, Vector3I plantLocation, Tile plantTile, Vector3I soilLocation, Tile soilTile, EntityUniverseFacade universe, bool weatherWatered, bool sprinklerWatered) {
	        return Instance.PlantLogic.DailyVisit(plantBlob, plantLocation, plantTile, soilLocation, soilTile, universe,
	            weatherWatered, sprinklerWatered);
	    }

        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose() { }
	    public void GameContextInitializeInit() { }
	    public void GameContextInitializeBefore() { }
	    public void GameContextInitializeAfter() { }
	    public void GameContextDeinitialize() { }
	    public void GameContextReloadBefore() { }
	    public void GameContextReloadAfter() { }
	    public void UniverseUpdateBefore(Universe universe, Timestep step) { }
	    public void UniverseUpdateAfter() { }
	    public bool CanPlaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
	        return true;
	    }

	    public bool CanReplaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
	        return true;
	    }

	    public bool CanRemoveTile(Entity entity, Vector3I location, TileAccessFlags accessFlags) {
	        return true;
	    }

	    public void ClientContextInitializeInit() { }
	    public void ClientContextInitializeBefore() { }
	    public void ClientContextInitializeAfter() { }
	    public void ClientContextDeinitialize() { }
	    public void ClientContextReloadBefore() { }
	    public void ClientContextReloadAfter() { }
	    public void CleanupOldSession() { }
	    public bool CanInteractWithTile(Entity entity, Vector3F location, Tile tile) {
	        return true;
	    }

	    public bool CanInteractWithEntity(Entity entity, Entity lookingAtEntity) {
	        return true;
	    }
	}
}
